//+------------------------------------------------------------------+
//|                                                 CSToolAssit3.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+

input double topZone=0.905;
input double bottomZone= 0.89;
input double widthZone = 0.001;
input double lotSize=0.01;
input int percentOrderSizeForAutoTrade=10;
input bool hLine=false;

int openingOrderTotal=0;

int PositionIndex;
int positionArrayIndex;
bool queryPass;

double point;

double openingLotTotal=0;
double buySellLotTotal=0;
double pengindLotTotal=0;

double pointSummary=0;
double pipValue=0;
double pointValue=0;
double pointValueSummary=0;
double openingPointSummary=0;
double openingPointValueSummary=0;
double accountBalancePercent=0;

double zoneForCalculation=0;
double currentLotCanOpen=0;

double positionZone[800];
double i;
double x;
double y;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- create timer
   EventSetTimer(1);

// Broker digits
   point=Point;
   if((Digits==3) || (Digits==5))
     {
      point*=10;
     }

   positionArrayIndex=0;
   int aa=0;
   for(i=bottomZone; i<=topZone; i+=widthZone)
     {
      positionZone[positionArrayIndex]=NormalizeDouble(i,5);
      pointSummary+=(positionZone[positionArrayIndex]-bottomZone)*100000;
      positionArrayIndex++;
      if(aa%10==0 && hLine==true)
        {
         ObjectCreate("hLine"+i,OBJ_HLINE,0,Time[0],i,0,0);
         ObjectSet("hLine"+i,OBJPROP_COLOR,Blue);
        }
      aa++;
     }
   positionZone[positionArrayIndex]=NormalizeDouble(i,5);
   pointSummary+=(positionZone[positionArrayIndex]-bottomZone)*100000;

   pointSummary=NormalizeDouble(pointSummary,5);
   Print("Point Sumary = "+pointSummary);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   ObjectsDeleteAll(0,OBJ_HLINE);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTick()
  {

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTimer()
  {
//OnCalculation();
//OpenOrderFunction();

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//variable for OnOpenOrderCondition
bool rsiIsUnderedValue=false;
bool rsiIsOveredValue=false;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OpenOrderFunction()
  {
   if(!rsiIsUnderedValue) //if rsi is going down < 22
     {
      if(iRSI(NULL,0,14,0,0)<=22)
        {
         rsiIsUnderedValue=true;
        }
     }

   if(rsiIsUnderedValue=true) //after rsi < 22 check rsi going up to 29
     {
      if(iRSI(NULL,0,14,0,0)>=29)
        {
         //validate before open order
         //check current priceing was in zone?

         if(!CheckOrderIsOpenedInZone())
           {
            // if this zone doesn't hav order open order (create calculation lot size function) and set rsiIsUnderedValue = false
            OrderSend(Symbol(),OP_BUY,LotCalculateForOpenOrder(),Ask,3,0,0,"My order",16384,0,clrGreen);
           }
         // if this zone already hav order set rsiIsUnderedValue = false
         rsiIsUnderedValue=false;
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CloseOrderFunction()
  {
   if(!rsiIsOveredValue) //if rsi is going over > 78
     {
      if(iRSI(NULL,0,14,0,0)>=78)
        {
         rsiIsOveredValue=true;
        }
     }

   if(rsiIsOveredValue=true) //after rsi > 78 check rsi going down to 71
     {
      if(iRSI(NULL,0,14,0,0)<=71)
        {
         openingOrderTotal=OrdersTotal();
         for(PositionIndex=openingOrderTotal-1; PositionIndex>=0; PositionIndex --) //  <-- for loop to loop through all Orders . .   COUNT DOWN TO ZERO !
           {
            queryPass=!OrderSelect(PositionIndex,SELECT_BY_POS,MODE_TRADES);
            if(queryPass) continue;   // <-- if the OrderSelect fails advance the loop to the next PositionIndex
              {
                 if(OrderProfit()>0.01){
                 OrderClose(OrderTicket(),OrderLots(),Ask,3,Red);
                 }
              }
           }

         //validate before close order
         //close every order that hav profit 
         // if this zone already hav order set rsiIsUnderedValue = false
         // if this zone doesn't hav order open order (create calculation lot size function) and set rsiIsUnderedValue = false

         rsiIsOveredValue=false;
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CheckOrderIsOpenedInZone()
  {
   openingOrderTotal=OrdersTotal();
   int amountOfOpenedOrderInZone=0;
   positionArrayIndex=0;
   while(positionZone[positionArrayIndex]<Ask)
     {
      positionArrayIndex++;
     }

   for(PositionIndex=openingOrderTotal-1; PositionIndex>=0; PositionIndex --) //  <-- for loop to loop through all Orders . .   COUNT DOWN TO ZERO !
     {
      queryPass=!OrderSelect(PositionIndex,SELECT_BY_POS,MODE_TRADES);
      if(queryPass) continue;   // <-- if the OrderSelect fails advance the loop to the next PositionIndex
        {
         if(OrderOpenPrice()<positionZone[positionArrayIndex] && OrderOpenPrice()>positionZone[positionArrayIndex-1])
           {
            amountOfOpenedOrderInZone++;
           }
        }
     }

   if(amountOfOpenedOrderInZone==0)
     {
      return false;
     }
   else
     {
      return true;
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double LotCalculateForOpenOrder()
  {
   int handlePercentOrderSizeForAutoTrade= percentOrderSizeForAutoTrade;
   if(handlePercentOrderSizeForAutoTrade==0|| handlePercentOrderSizeForAutoTrade>99)
     {
      handlePercentOrderSizeForAutoTrade=1;
     }

   if(currentLotCanOpen*(handlePercentOrderSizeForAutoTrade/100)>0.01)
     {
      return currentLotCanOpen*(handlePercentOrderSizeForAutoTrade/100);
     }
   return 0.01;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnCalculation()
  {
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*point)/MarketInfo(Symbol(),MODE_TICKSIZE))*lotSize);
   pointValue=pipValue/10;
   pointValueSummary=pointSummary*pointValue;

   openingOrderTotal=OrdersTotal();
   openingLotTotal=0;
   buySellLotTotal=0;
   pengindLotTotal=0;
   openingPointSummary=0;
   zoneForCalculation=0;

   for(PositionIndex=openingOrderTotal-1; PositionIndex>=0; PositionIndex --) //  <-- for loop to loop through all Orders . .   COUNT DOWN TO ZERO !
     {
      queryPass=!OrderSelect(PositionIndex,SELECT_BY_POS,MODE_TRADES);
      if(queryPass) continue;   // <-- if the OrderSelect fails advance the loop to the next PositionIndex
        {

         openingLotTotal+=OrderLots();

         if(OrderType()==0 || OrderType()==1)
           {
            buySellLotTotal+=OrderLots();
           }
         else
           {
            pengindLotTotal+=OrderLots();
           }
        }

     }

   y=topZone -(openingLotTotal/10);    //todo check digit if jpy
   for(x=topZone; x>y;x-=widthZone)
     {
      x=NormalizeDouble(x,5);
      openingPointSummary+=(x-bottomZone)*100000;
     }
   openingPointValueSummary=openingPointSummary*pointValue;

   accountBalancePercent=openingPointValueSummary/(AccountBalance()*100);

   if(hLine==true)
     {
      int aa=0;
      double bb=0;
      bb=topZone-(widthZone*(openingLotTotal*100));
      for(i=bottomZone; i<=topZone; i+=widthZone)
        {
         if(aa%10==0 && i>bb)
           {
            ObjectSet("hLine"+i,OBJPROP_COLOR,Red);
              }else{
            ObjectSet("hLine"+i,OBJPROP_COLOR,Blue);
           }
         aa++;
        }
     }

   for(i=topZone; i>=Ask; i-=widthZone)
     {
      zoneForCalculation++;
     }

   currentLotCanOpen=(zoneForCalculation*lotSize)-openingLotTotal;

//Comment("Opening Order Total  = "+openingOrderTotal
//        +"\nOpening Lot Total = "+DoubleToStr(openingLotTotal,2)
//        +"\nBuy Sell Lot Total   = "+DoubleToStr(buySellLotTotal,2)
//        +"\nPending Lot Total = "+DoubleToStr(pengindLotTotal,2)
//        +"\nPoint Value Summary  = "+DoubleToStr(pointValueSummary,0)+"("+DoubleToStr((pointValueSummary/200),0)+")"
//        +"\nPoint Opening Point Value Summary   = "+DoubleToStr(openingPointValueSummary,0)+"("+DoubleToStr((openingPointValueSummary/200),0)+")"
//        +"\nAccount Balance Percent   = "+DoubleToStr(accountBalancePercent,2)+"%"
//        +"\nLot Can Open  = "+DoubleToStr(currentLotCanOpen,2));

  }
//+------------------------------------------------------------------+
